function Calendar(config) {
    this.container = document.getElementById(config.container);
    this.container.classList.add('calendar');
    this.today = new Date();
    this.selected = this.today;
    this.currentYear = this.today.getFullYear();
    this.months = config.months;
    this.weekDays = config.weekDays;
    this.monthDays = config.monthDays;

    this.getPrevDays = (date, staDay=0)=>{
        let ret = [];
        let year = date.getFullYear();
        let month = date.getMonth();
        let firstWeekday =  new Date(year, month, 1).getDay();
        let days = (firstWeekday + 7) - (staDay +7) - 1;
        for (let i=days * -1; i<=0;i++){
            ret.push({date:new Date(year, month, i).getDate(), type:"not-current", id:new Date(year, month, i) });
        }
        return ret;
    }

    this.getNextDays = (prevMonthDays, monthDays)=>{
        let ret = [];
        let days = 42 - (prevMonthDays.length + monthDays.length);
        for(let i = 1; i<=days; i++){
            ret.push({date:i, type:"not-current"});
        }
        return ret;
    }

    this.getCurrentDays = (date)=>{
        let ret = [];
        let year = date.getFullYear();
        let month = date.getMonth();
        let lastDay = new Date(year, month +1 , 0).getDate();
        for(let i = 1; i<=lastDay;i++){
            ret.push({date:i, type:"current", id:this.YYYYmmdd(new Date(year, month, i)), num_day: new Date(year, month, i).getDay()});
        }
        return ret;
    }

    this.YYYYmmdd = (date)=>{
        let d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
        return [year, month, day].join('-');
    }

    this.calendarBody = (year)=>{
        year = this.currentYear;
        let calendarBody = document.createElement('div');
        calendarBody.classList.add('calendar-body');

        var monthDaysBody = document.createElement('div');
        monthDaysBody.classList.add('month-days');
        monthDaysBody.classList.add('row');
        // calendarBody = document.createElement('div');
        let monthbody = document.createElement('div');
        monthbody.setAttribute('id', 'month');
        monthbody.classList.add('month');
        monthbody.classList.add('col');
        let monthhtml = document.createElement('span');
        monthhtml.innerHTML = '';
        monthbody.appendChild(monthhtml);
        monthDaysBody.appendChild(monthbody);

        for(let d = 1; d<=this.monthDays;d++){
            let cell = document.createElement('div');
            cell.classList.add('day');
            cell.classList.add('col');
            let day = document.createElement('span');
            day.innerHTML = d;
            cell.appendChild(day);
            monthDaysBody.appendChild(cell);
            calendarBody.appendChild(monthDaysBody);
        }
        this.container.appendChild(calendarBody);

        for(let i = 0; i<this.months.length;i++){
            var monthDaysBody = document.createElement('div');
            monthDaysBody.classList.add('month-days');
            monthDaysBody.classList.add('row');
            // calendarBody = document.createElement('div');
            let monthbody = document.createElement('div');
            monthbody.setAttribute('id', i+1);
            monthbody.classList.add('month');
            monthbody.classList.add('col');
            let monthhtml = document.createElement('span');
            monthhtml.innerHTML = this.months[i];
            monthbody.appendChild(monthhtml);
            monthDaysBody.appendChild(monthbody);
            let date = new Date(year, i+1, 0);
            let daysThisMonth = this.getCurrentDays(date);
            [...daysThisMonth]
                .forEach(num=>{
                    console.log(num);
                    let cell = document.createElement('div');
                    cell.setAttribute('id', num.id);
                    cell.classList.add('day');
                    cell.classList.add('col');
                    let day = document.createElement('span');

                    day.innerHTML = this.weekDays[num.num_day];
                    cell.appendChild(day);
                    num.type === 'not-current'?cell.classList.add('not-current'):cell.classList.add('current');
                    if(num.id === this.YYYYmmdd(this.today)){
                        cell.classList.add('active');
                        cell.classList.add('selected');
                    }
                    if(num.num_day === 0 || num.num_day === 6) {
                        cell.classList.add('weekend');
                    }
                    monthDaysBody.appendChild(cell);

                });
            var lastDay = new Date(year, i+1 , 0).getDate();
            var dopCol = this.monthDays - lastDay;
            if(dopCol != 0){
                for (let i = 0; i<dopCol;i++){
                    let cell = document.createElement('div');
                    cell.classList.add('day');
                    cell.classList.add('col');
                    let day = document.createElement('span');
                    cell.appendChild(day);
                    monthDaysBody.appendChild(cell);
                }
            }
            calendarBody.appendChild(monthDaysBody);

            console.log(lastDay);

            this.container.appendChild(calendarBody);
        }



    }

    this.showCalendar = (year)=>{
        this.container.innerHTML = '';
        this.calendarBody(year);
    }

    this.showCalendar(this.currentYear);
}